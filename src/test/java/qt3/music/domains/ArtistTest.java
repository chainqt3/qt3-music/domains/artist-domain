package qt3.music.domains;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ArtistTest {

    @Test
    void createArtistSuccess() {
        var song = new Artist.Song(UUID.randomUUID().toString(), "title");
        var album = new Artist.Album(UUID.randomUUID().toString(), "name", List.of(song).toArray(Artist.Song[]::new));

        String id = UUID.randomUUID().toString();
        Artist artist = Artist.builder()
                .id(id)
                .name("name")
                .albums(List.of(album).toArray(Artist.Album[]::new))
                .build();

        assertEquals(id, artist.id);
        assertEquals("name", artist.name);
        assertEquals(1, artist.getAlbums().length);
    }
}
