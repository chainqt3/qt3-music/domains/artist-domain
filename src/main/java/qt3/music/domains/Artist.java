package qt3.music.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document
public class Artist implements Serializable {
    @Id String id;
    String name;
    Album[] albums;

    public record Album(@Id String id, String name, Song[] songs) implements Serializable {}

    public record Song(@Id String id, String title) implements Serializable {}
}

